from PyQuantumWire.material import Material
from PyQuantumWire.csv_reader import read_setup
from PyQuantumWire.multigraph_plotter import multiple_graphs
import numpy as np


if __name__ == "__main__":
    # Change the values below as you pleased!
    # Change example.csv in the csv directory to
    # setup.csv
    setup_file = "csv/setup.csv"

    # Define experiment material. This can be
    # a custom constructor of any material or
    # any build in material such as GaAs.
    material = Material.gallium_arsenide()

    # Number of steps in the G curve.
    no_of_bands = 5

    # Gap between plots.
    offset = 4

    # Reading the setups and generating graph.
    setup = read_setup(setup_file)

    # View the material detail.
    material.view_material()
    print()

    # Plot the conductance curves.
    multiple_graphs(no_of_bands, material, setup, offset)
