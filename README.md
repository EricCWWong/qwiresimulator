# Quantum Wire Simulation
This repository includes python function that will allow user to simulate the quantuzed conductance of quantum wires with non-interacting electrons.

## Basic Usage
The `main.py` file can be run to generate multiple conductance plot in the same graph without installation of the PyQuantumWire module. There are 2 major things that users can change:

1) The material class, number of bands and offsets between curves can be adjusted according to the experimental setup and graph requirements in the `main.py`. The default setup is: GaAs (Material), 5 bands and 2 units of offset. 
2) The `setup.csv` in the `csv` folder. Inside the csv file, users can change the configuration of constriction, source-drain voltage and magnetic field.

## PyQuantumWire
If user would like to use the functions to further expand the reach of the simulations, users can install the package using pip. To install the package, simply run inside the terminal:

`pip3 install .`

To unistall, run:

`pip3 uninstall PyQuantumWire`

## Features to be added soon
- Break and report when perpendicular magnetic field is too strong and plateus become too big.
- Adding custom energy offset to each energy band.
- Contour plot of the conductance curve.
- Website UI version.

## Web Version
Coming soon.

## Contribute
Feel free to fork this repository and add any new features. If bugs are found, please report in issue page.

## Model Reference
- Büttiker, M., 1990. Quantized transmission of a saddle-point constriction. Physical Review B, 41(11), p.7906.
- Martin-Moreno, L., Nicholls, J.T., Patel, N.K. and Pepper, M., 1992. Non-linear conductance of a saddle-point constriction. Journal of Physics: Condensed Matter, 4(5), p.1323.
- Patel, N.K., Nicholls, J.T., Martn-Moreno, L., Pepper, M., Frost, J.E.F., Ritchie, D.A. and Jones, G.A.C., 1991. Properties of a ballistic quasi-one-dimensional constriction in a parallel high magnetic field. Physical Review B, 44(19), p.10973.