from setuptools import setup, find_packages

setup(
    name="PyQuantumWire",
    packages=find_packages(exclude=['*test']),
    version="0.1.1",
    author="Eric Wong",
    description='This package allows user to \
        simulate conductance of quantum wires',
    author_email='ericwongchunwing@gmail.com',
    install_requires=['numpy', 'matplotlib', 'prettytable'],
)
