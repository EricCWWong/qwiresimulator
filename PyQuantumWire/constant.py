"""
This file includes all the important physical constants.
"""

# Reduced Planck's constant 1.05 * 10ˆ(-34) Js.
h_bar = 1.05 * 10**(-34)

# Electron mass 9.11 * 10ˆ(-31) kg.
m_e = 9.11 * 10**(-31)

# Electric charge (Q).
e = 1.6 * 10**(-19)

# Bohr magneton 5.79 * 10ˆ(-5) eV = 5.79 * 10ˆ(-2) meV
mu_b = 5.79 * 10**(-2)
