import numpy as np


def read_setup(file_name):
    """
    This will take a .csv file and retrieve the experimental setup.

    Parameters
    ----------
    filename: str
        The filename that contains the experiment setup in .csv format.
    Returns
    -------
    exp_setup: array
        This is the array for experiment setup.
    """

    exp_setup = np.genfromtxt(
        file_name,
        delimiter=",",
        skip_header=1)

    if exp_setup.ndim == 1:
        exp_setup = [exp_setup]
        exp_setup = np.array(exp_setup)
    return exp_setup
