from matplotlib import pyplot as plt
from prettytable import PrettyTable
from .maths_func import round_off
from .saddle_point_model import SaddlePointModel
import numpy as np


def multiple_graphs(no_of_bands, material, setup, offset):
    """
        This function generates multiple conductance curves
        in the same graph.

        Parameter
        ---------
        no_of_bands : int
            This is the number of bands the curves will show.
        material : Material
            This is a Material object that encodes the data
            of the specific material.
        setup : 2D - Array
            Setup[3][i] indicates the i-th property of the
            3rd quantum wire setup. The i values corresponds to:
            i = 0 ==> hw_x (meV)
            i = 1 ==> w_y / w_x
            i = 2 ==> Vsd (mV)
            i = 3 ==> magnetic field (T)
            i = 4 ==> magnetic field angle (deg) (0 deg ==> parallel)
        offset : float
            Offset is the gap between conductance curves.

        Returns
        -------
        Conductance Plot
    """

    # Number of graphs:
    no_of_graphs = len(setup)

    # Initialise graph:
    fig, axs = plt.subplots(1, 1)
    fig.suptitle("Conductance Curve of " + material.name)
    axs.set_ylabel(r'$G \times \frac{h}{2e^{2}} $')
    axs.set_xlabel(r'$ \frac{E_{f} - U_{0}}{\hbar w_{x}}$')
    axs.set_ylim([0, no_of_bands])
    axs.set_yticks(np.arange(0, no_of_bands + 0.25, 0.25))
    plt.grid()

    # Initialise table:
    table = PrettyTable()
    table.field_names = ['hbar w_x (meV)', 'w_y/ w_x', 'B (T)',
                         'angle (rad)', 'E1 (meV)', 'E2 (meV)',
                         'eVsd (meV)', 'hbar w_c (meV)', 'Zeeman (meV)']

    # Plotting
    for i in range(no_of_graphs):
        # Reading the i th setup
        setup_i = setup[i]

        # Assign array data into single variables
        hw_x = setup_i[0]
        hw_y = setup_i[1] * setup_i[0]
        eVsd = setup_i[2]
        magnetic_field = setup_i[3]
        field_angle = setup_i[4]

        # Constructing a SaddlePointModel
        model = SaddlePointModel(material, hw_x, hw_y, eVsd,
                                 magnetic_field, field_angle)

        # adding row data in table
        table.add_row(model.get_exp_setup())

        gap = i * offset
        model.generate_graph(no_of_bands, offset=gap, output=False,
                             fig_return=axs)

    # Show table
    print(table)

    # Show plot
    plt.show()
