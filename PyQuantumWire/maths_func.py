import numpy as np
from math import floor, pow, ceil


def round_off(N, n):
    """
        This function rounds number N to n significant figure.

        Parameter
        ---------
        N : float
            The number to be rounded.
        n : int
            The number of significant number.

        Return
        ------
        j : The rounded number.
    """
    b = N
    c = floor(N)

    # Counting the no. of digits
    # to the left of decimal point
    # in the given no.
    i = 0
    while(b >= 1):
        b = b / 10
        i = i + 1

    d = n - i
    b = N
    b = b * pow(10, d)
    e = b + 0.5
    if (float(e) == float(ceil(b))):
        f = (ceil(b))
        h = f - 2
        if (h % 2 != 0):
            e = e - 1
    j = floor(e)
    m = pow(10, d)
    j = j / m

    return j


def heaviside_step_function(theta):
    """
        This function returns value of the Heaviside
        step function with an argument theta. More
        precisely, this function returns:
        1/(1 + exp(theta))

        Paramter
        --------
        theta : float
            Theta is the value of the argument of the Heaviside
            step function.
        Returns
        -------
        heaviside : float
            The value of the Heaviside step function.
    """
    heaviside = 1/(1 + np.exp(theta))

    return heaviside
