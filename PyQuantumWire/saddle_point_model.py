import numpy as np
from numpy import pi, sin, exp, sqrt, radians
from matplotlib import pyplot as plt
from .constant import h_bar, e
from .material import Material
from .maths_func import heaviside_step_function, round_off

"""
    This class models the quantised conductance of
    a saddle-point set-up.
"""


class SaddlePointModel:
    def __init__(self, material, hw_x, hw_y, Vsd=0, magnetic_field=0, angle=0):
        """
            This is the constructor of a non-interacting saddle-point model.

            Parameters
            ----------
            material : Material
                This is an object that encodes all the essential data of
                the material of the saddle-point quantum wire.
            hw_x : float
                w_x is the term that defines the curvature of the electric
                potential at the saddle. In particular, w_x describes the
                width.
            hw_y : float
                w_y is the term that defines the curvature of the electric
                potential at the saddle. In particular, w_y describes the
                seperation of the quantum channels.
            Vsd : float
                Vsd is the source-drain voltage.
            magnetic_field : float
                This is the magnitude of the magnetic field the quantum
                wire system experience.
            angle : float
                This the the angle of the magnetic field with respect to
                the system. In particular, perpendicular field is 90 deg
                and parallel field is 0 deg. The unit is degrees.
        """
        self.material = material
        assert type(self.material) == Material, "Not correct type"
        self.hw_x = hw_x
        self.hw_y = hw_y
        self.eVsd = Vsd     # Note Vsd is in mV ==> eVsd in meV
        self.magnetic_field = magnetic_field
        self.angle = radians(angle)

        # Parameters from perpendicular B field:
        self.hw_c = (h_bar * sin(self.angle) * self.magnetic_field /
                     self.material.electron_eff_mass) * 10**(3)

        self.angular_freq = self.hw_c**2 + self.hw_y**2 - self.hw_x**2

        # Energy parameters:
        self.E1 = ((self.angular_freq**2
                    + 4 * self.hw_x**2 * self.hw_y**2)**(1/2)
                   - self.angular_freq)**(1/2) * 1/(2 * pi * sqrt(2))

        self.E2 = 1/(sqrt(2)) * ((self.angular_freq**2
                                  + 4 * self.hw_x**2 * self.hw_y**2)**(1/2)
                                 + self.angular_freq) ** (1/2)

        # Zeeman values:
        self.zeeman_up = self.material.zeeman_spin_up(self.magnetic_field)
        self.zeeman_down = self.material.zeeman_spin_down(self.magnetic_field)

    def forward_transmission(self, n, x):
        """
            This captures the forward transmission rate of electrons through
            the saddle.

            Parameters
            ----------
            n: int
                The nth channel.
            x: float
                The x value on the graph, in this case roughly proportional to
                Fermi energy.
            Returns
            -------
            t_forward: float
            Returns the forward transmission rate of the nth channel.
        """

        arg_spin_up = -1/self.E1 * (self.hw_x * x + 1/2 * self.eVsd -
                                    (n + 1/2) * self.E2 + self.zeeman_up)

        arg_spin_down = -1/self.E1 * (self.hw_x * x + 1/2 * self.eVsd -
                                      (n + 1/2) * self.E2 + self.zeeman_down)

        t_forward = 1/2 * heaviside_step_function(arg_spin_up) + \
            1/2 * heaviside_step_function(arg_spin_down)

        return t_forward

    def backward_transmission(self, n, x):
        """
            This captures the backward transmission rate of electrons through
            the saddle.

            Parameters
            ----------
            n: int
                The nth channel.
            x: float
                The x value on the graph, in this case roughly proportional to
                Fermi energy.
            Returns
            -------
            t_backward: float
            Returns the backward transmission rate of the nth channel.
        """

        arg_spin_up = -1/self.E1 * (self.hw_x * x - 1/2 * self.eVsd -
                                    (n + 1/2) * self.E2 + self.zeeman_up)

        arg_spin_down = -1/self.E1 * (self.hw_x * x - 1/2 * self.eVsd -
                                      (n + 1/2) * self.E2 + self.zeeman_down)

        t_backward = 1/2 * heaviside_step_function(arg_spin_up) + \
            1/2 * heaviside_step_function(arg_spin_down)

        return t_backward

    def total_transmission(self, no_of_bands, x):
        """
            This will calculate the total transmission rate for all bands,
            calculated by summing all the transmissions of individual bands.

            Parameters
            ----------
            no_of_bands : int
                The number of bands the graph will consider.
            x: float
                The x value of the graph, this is roughly proportional to
                Fermi energy.

            Returns
            -------
            transmission : float
                Returns the total transmission rate of all bands.
        """
        transmission = 0

        for i in range(0, no_of_bands):
            transmission = transmission + self.forward_transmission(i, x) + \
                self.backward_transmission(i, x)
        return 1/2 * transmission

    def generate_graph(self, no_of_bands, offset=0, output=True,
                       fig_return=None):
        """
            This function generates the conductance curve of this quantum
            wire setup.

            Parameters
            ----------
            no_of_bands : int
                The number of bands the graph will consider.

            Returns
            -------
            Conductance Curve
        """

        # Initialise graph:
        if output:
            fig, axs = plt.subplots(1, 1)
            fig.suptitle("Conductance Curve of " + self.material.name)
            axs.set_ylabel(r'$G \times \frac{h}{2e^{2}} $')
            axs.set_xlabel(r'$ \frac{E_{f} - U_{0}}{\hbar w_{x}}$')
            axs.set_ylim([0, no_of_bands])
            axs.set_yticks(np.arange(0, no_of_bands + 0.25, 0.25))
            plt.grid()
        else:
            axs = fig_return

        x = np.arange(-2, no_of_bands * 6, 0.1)
        y = self.total_transmission(no_of_bands, x)
        x = np.arange(-2, no_of_bands * 6, 0.1) + offset
        axs.plot(x, y)

        for j, el in enumerate(y):
            if el >= no_of_bands - 0.1:
                x_max = x[j]
                break

        axs.set_xlim([-1, x_max + 1])

        if output:
            plt.show()

    # The following functions allow you to get important set-up info.
    def get_exp_setup(self):
        setup = [self.hw_x, self.hw_y/self.hw_x, self.magnetic_field,
                 round_off(self.angle, 3),
                 round_off(self.E1, 3), round_off(self.E2, 3),
                 self.eVsd,
                 round_off(self.hw_c, 3),
                 round_off(self.zeeman_up, 3)]
        return setup
