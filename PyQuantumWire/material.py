from .constant import m_e, mu_b

"""
    This class encodes the data of a material (such as GaAs).
    This material will be the material of a quantum wire setup.
"""


class Material:
    def __init__(self, name, g, m_eff):
        """
            This is the constructor of the class -- Material.
            This class is constructed by the name, the Lande g-
            factor and the effective mass of electron inside
            this material.

            Parameters
            ----------
            name : string
                This is the chemical formula of the material.
                For example, if the targetted material is
                Gallium Arsenide, then you should input name as
                "GaAs".

            g : float
                This is the value of the Lande g-factor. It is a
                float and has no unit.

            m_eff : float
                This is the effective mass factor relative
                to the rest mass of electrons.

                Do not input the exact effective mass. m_eff is the
                factor value in the following equation:
                exact effective mass = factor * electron rest mass
        """
        self.name = name
        self.electron_eff_mass_factor = m_eff
        self.electron_eff_mass = m_eff * m_e
        self.lande_g = g

    # The following are constructors that can construct common materials
    # such as GaAs. If you want to add new material, please submit an issue.
    @classmethod
    def gallium_arsenide(cls):
        gaas = cls(name="GaAs", g=0.04, m_eff=0.067)
        return gaas

    # The following methods allow us to change the values of the class.
    def set_lande_g(self, new_g):
        self.lande_g = new_g

    # The following methods allow us to view to get or view the material data.
    def get_lande_g(self):
        return self.lande_g

    def get_name(self):
        return self.name

    def get_electron_eff_mass(self):
        return self.electron_eff_mass

    def get_electron_eff_mass_factor(self):
        return self.electron_eff_mass_factor

    def view_material(self):
        print("Material         :", " " + self.get_name())
        print("Lande g-factor   :", " " + str(self.get_lande_g()))
        print("Eff mass factor  :", " " +
              str(self.get_electron_eff_mass_factor()))

    # The following are methods allowing us to calculate energy terms of this
    # material.
    def zeeman_spin_up(self, B, spin=1/2):
        zeeman_term = self.lande_g * mu_b * B * spin
        return zeeman_term

    def zeeman_spin_down(self, B, spin=-1/2):
        zeeman_term = self.lande_g * mu_b * B * spin
        return zeeman_term
